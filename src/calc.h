#include <iostream>
#include <stack>

struct leksema {
	char type;
	double value;
};

/*
 *	Function for calculating a binary mathematical expression.
 *-----------------------------------------------------
 *	Input data:
 *	Stack_N - Stack of values
 *	Stack_O - Operation stack
 *	item - Buffer token for adding an item to the value stack of the result of the operation performed
 */
bool Maths(std::stack<leksema> &Stack_N, std::stack<leksema> &Stack_O, leksema &item);

/*
 *	Function of calculating the priority of the performed operation
 *------------------------------------------------- ----
 *	Input data:
 *	ch - operation character
 */
int getRange(char ch);

/*
 *	Exponentiation function
 *  *-----------------------------------------------------
 *	Input data:
 *	base - Number raised to a power
 *	exp - The degree to which the number must be raised
 */
double pow(double base, double exp);
