#include "calc.h"

bool Maths(std::stack<leksema> &Stack_N, std::stack<leksema> &Stack_O, leksema &item) {
	double a, b, c;
	a = Stack_N.top().value;
	Stack_N.pop();
	b = Stack_N.top().value;
	Stack_N.pop();

	switch (Stack_O.top().type) {
	case '+':
		c = a + b;
		break;
	case '-':
		c = b - a;
		break;
	case '*':
		c = a * b;
		break;
	case '/':
		if (a == 0) {
			std::cerr << "\n���������� ������� �� 0\n";
			return false;
		} else {
			c = b / a;
			break;
		}
	case '^':
		c = pow(b, a);
		break;
	default:
		std::cerr << "\n�������� ����\n";
		return false;
	}

	item.type = '0';
	item.value = c;
	Stack_N.push(item);
	Stack_O.pop();
	return true;
}

int getRange(char ch) {
	if ((ch == '+') || (ch == '-'))
		return 1;
	if ((ch == '*') || (ch == '/'))
		return 2;
	if (ch == '^')
		return 3;
	else
		return 0;
}

double pow(double base, double exp){
    if (exp==1)
        return base;
    else{
        exp--;
        return (base*pow(base,exp));
    }
}

