#include "calc.h"

int main(void) {
	setlocale(0, "RUS");
	bool flag = 1;//	Flag for specifying the minus at the beginning of the formula
	std::stack<leksema> Stack_N;
	std::stack<leksema> Stack_O;
	leksema item;

	std::cout
			<< "	Добро пожаловать в калькулятор!!!\n	Введите формулу:";
	char ch; 						//	Input character buffer
	while (true) {
		ch = std::cin.peek();
		if (ch == '\n' && Stack_N.size() == 0) {//	If the end of the line and the stack is empty
			std::cerr << "Введите что-нибудь!";
			return 0;
		}
		if (ch == '\n')					//	If end of line completion of input
			break;
		if (ch == ' ') {
			std::cin.ignore();					//	Ignoring spaces in a formula
			continue;
		}

		if ((ch >= '0' && ch <= '9') || (ch == '-' && flag == 1)) {
			std::cin >> item.value;
			item.type = 0;
			Stack_N.push(item);
			flag = 0;
			continue;
		}

		if (ch == '+' || (ch == '-' && flag == 0) || ch == '*' || ch == '/'
				|| ch == '^') {
			if (Stack_O.size() == 0) { 	//	If the stack is empty
				item.type = ch;
				item.value = 0;
				Stack_O.push(item);
				std::cin.ignore();
				continue;
			}
			if (Stack_O.size() != 0	//	If the stack is not empty and the priority of the operation is higher than the last one on the stack
			&& getRange(ch) > getRange(Stack_O.top().type)) {
				item.type = ch;
				item.value = 0;
				Stack_O.push(item);
				std::cin.ignore();
				continue;
			}
			if (Stack_O.size() != 0	//	If the stack is not empty and the priority of the operation is lower or equal to the last one on the stack
			&& getRange(ch) <= getRange(Stack_O.top().type)) {
				if (Maths(Stack_N, Stack_O, item) == false) {
					system("pause");
					return 0;
				}
				continue;
			}
		}
		if (ch == '(') {
			item.type = ch;
			item.value = 0;
			Stack_O.push(item);
			std::cin.ignore();
			continue;
		}
		if (ch == ')') {
			while (Stack_O.top().type != '(') {
				if (Maths(Stack_N, Stack_O, item) == false) {
					system("pause");
					return 0;
				} else
					continue;
			}
			Stack_O.pop();
			std::cin.ignore();
			continue;
		} else {
			std::cout << "\nОшибка ввода!\n";
			system("pause");
			return 0;
		}
	}

	while (Stack_O.size() != 0) {
		if (Maths(Stack_N, Stack_O, item) == false) {
			system("pause");
			return 0;
		} else
			continue;
	}
	std::cout << "	Результат вычислений: " << Stack_N.top().value << std::endl;
	return 0;
}

